// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import VModal from 'vue-js-modal'
import * as GoogleMaps from 'vue2-google-maps'

const moment = require('moment')
require('moment/locale/ru')
moment.locale('ru')

Vue.config.productionTip = false

Vue.use(require('vue-moment'), { moment })
Vue.use(VModal)
Vue.use(GoogleMaps, {
  load: {
    key: 'AIzaSyDIafrpFGkL2wJSxy_yAYm3giwb7Awc6XY',
    libraries: 'places'
  }
})

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  components: { App },
  template: '<App/>'
})
