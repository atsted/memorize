import httpClient from './httpClient'

const baseURL = '/reports'
const headers = { 'Content-Type': 'multipart/form-data' }

const setDefaultCallbackIfUndefined = fn => {
  return typeof fn === 'function' ? fn : console.log
}

export default {
  getAll (success, failure) {
    success = setDefaultCallbackIfUndefined(success)
    failure = setDefaultCallbackIfUndefined(failure)
    return httpClient.get(baseURL)
      .then(response => success(response))
      .catch(error => failure(error))
  },
  getById (id, success, failure) {
    success = setDefaultCallbackIfUndefined(success)
    failure = setDefaultCallbackIfUndefined(failure)
    return httpClient.get(`${baseURL}/${id}`)
      .then(response => success(response))
      .catch(error => failure(error))
  },
  create (params = {}, success, failure) {
    const formData = new FormData()
    for (let name in params) {
      if (name !== 'media') {
        formData.append(name, params[name])
      }
    }
    const media = params.media
    for (let i = 0; i < media.length; i++) {
      formData.append('media', media[i])
    }
    success = setDefaultCallbackIfUndefined(success)
    failure = setDefaultCallbackIfUndefined(failure)
    return httpClient.post(baseURL, formData, { headers })
      .then(response => success(response))
      .catch(error => failure(error))
  },
  update (params = {}, success, failure) {
    const { id } = params
    success = setDefaultCallbackIfUndefined(success)
    failure = setDefaultCallbackIfUndefined(failure)
    return httpClient.put(`${baseURL}/${id}`, params)
      .then(response => success(response))
      .catch(error => failure(error))
  },
  delete (id, success, failure) {
    success = setDefaultCallbackIfUndefined(success)
    failure = setDefaultCallbackIfUndefined(failure)
    return httpClient.delete(`${baseURL}/${id}`)
      .then(response => success(response))
      .catch(error => failure(error))
  }
}
