import axios from 'axios'
import { baseURL, headers } from './config'

export default axios.create({
  baseURL, headers
})
